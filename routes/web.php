<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// tokopedia api route
Route::get('/order-tokped', 'tokopediaController@orderTokped')->name('orderTokped');
Route::get('/tracking/order/status', 'tokopediaController@orderStatus')->name('orderStatus');
Route::get('/chat-tokped', 'tokopediaController@chatTokped')->name('chatTokped');
Route::get('/chat-tokped1', 'tokopediaController@chatTokped1')->name('chatTokped1');
Route::get('admin/cetak/label/{kode}', 'tokopediaController@cetakLabel')->name('cetakLabel');
Route::get('/auto-accept-order', 'tokopediaController@AutoAcceptOrder')->name('AutoAcceptOrder');

// blibli
Route::get('/get-order-blibli', 'blibliController@getOrder')->name('getOrderBlibli');
Route::get('/get-order-status', 'blibliController@orderStatusBlibli')->name('orderStatusBlibli');

// shopee
Route::get('/create-auth/{id}', 'shopeeController@createAuth')->name('createAuth');
Route::get('/chat-create-auth/{id}', 'shopeeController@createChatAuth')->name('createChatAuth');
Route::get('/get-order', 'shopeeController@getShopeeOrder')->name('getShopeeOrder');
Route::get('/get-chat', 'shopeeController@shopeeChat')->name('shopeeChat');
Route::get('/afterRedirect', 'shopeeController@afterRedirect')->name('afterRedirect');
Route::get('/afterRedirectChat', 'shopeeController@afterRedirectChat')->name('afterRedirectChat');
Route::get('/shopeeAutoAccepOrder', 'shopeeController@shopeeAutoAccepOrder')->name('shopeeAutoAccepOrder');


Route::get('/admin/products', 'PageController@products')->name('products');
