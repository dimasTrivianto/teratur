<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Carbon\Carbon;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call('App\Http\Controllers\tokopediaController@AutoAcceptOrder')->everyFiveMinutes();
      $schedule->call('App\Http\Controllers\tokopediaController@orderTokped')->everyFiveMinutes();
      $schedule->call('App\Http\Controllers\shopeeController@getShopeeOrder')->everyFiveMinutes();
      $schedule->call('App\Http\Controllers\shopeeController@shopeeChat')->everyMinute();
      $schedule->call('App\Http\Controllers\shopeeController@shopeeAutoAccepOrder')->everyFiveMinutes();
      $schedule->call('App\Http\Controllers\blibliController@getOrder')->everyFiveMinutes();
      $schedule->call('App\Http\Controllers\blibliController@orderStatusBlibli')->hourly();
      $schedule->call('App\Http\Controllers\tokopediaController@chatTokped')->everyMinute()->when(function () {
            return Carbon::now()->format('i') % 2 == 1;
      });
      $schedule->call('App\Http\Controllers\tokopediaController@chatTokped1')->everyMinute()->when(function () {
            return Carbon::now()->format('i') % 2 == 0;
      });
      $schedule->call('App\Http\Controllers\tokopediaController@orderStatus')->hourly();

    
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
