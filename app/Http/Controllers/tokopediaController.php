<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Client;
use Carbon\Carbon;
use DB;

class tokopediaController extends Controller
{
   public function orderTokped() {
    
        $notifSendTo = DB::table('cms_users')->where(function ($query) {
            $query->where('id_cms_privileges', '=', 1)
                  ->orWhere('id_cms_privileges', '=', 2);
                })
                  ->pluck('id');
        
        // dd($notifSendTo);
        
        $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");

        $appID = env('TOKOPEDIA_APP_ID');
        try{
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        }catch (RequestException $e) {
            // dd(Psr7\str($e->getRequest()));
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        $token = json_decode($res);

        // tokped order api
        date_default_timezone_set("Asia/Jakarta");

        $current_time = Carbon::now()->timestamp + 86400;
        $start_time = Carbon::now()->timestamp - 86400;
        
        $tokpedOrders = [];
        $shopsData = [
            'warisan Sriwijaya' => [
                'shopsId' => 10696883,
                'to' => "https://www.tokopedia.com/warisansriwijaya"
            ],
            'Warisasn Hayam Wuruk' => [
                'shopsId' => 9040202,
                'to' => "https://www.tokopedia.com/warisanhayam"
            ],
            'Warisan Gajahmada' => [
                'shopsId' => 8383447,
                'to' => "https://www.tokopedia.com/warisangajahmada"
            ],
            'Warisan Gajahmada Solo' => [
                'shopsId' => 9226747,
                'to' => "https://www.tokopedia.com/wgajahmadasolo"
            ],
            'BARDI Banten Official'=>[
                'shopsId'=>11466905,
                'to'=>"https://www.tokopedia.com/bardibanten"
            ],
            'Warisan Gajahmada Yogyakarta' => [
                'shopsId' => 9235772,
                'to' => "https://www.tokopedia.com/Wgmyogya"
            ],
            'Warisasn Gajahmada Semarang' => [
                'shopsId' => 9212736,
                'to' => "https://www.tokopedia.com/warisangajahmadasemarang"
            ],
            'Warisan Gajahmada Cirebon' => [
                'shopsId' => 9585109,
                'to' => "https://www.tokopedia.com/Warisangajahmadacirebon"
            ],
            'Warisan Gajahmada Surabaya' => [
                'shopsId' => 9235772,
                'to' => "https://www.tokopedia.com/warisangajahmadasurabaya"
            ],
            'Warisan Gajahmada Denpasar' => [
                'shopsId' => 9583529,
                'to' => "https://www.tokopedia.com/Warisangajahmadadenpasar"
            ],
        ];

        try{
            foreach ($shopsData as $namaToko=> $key) {
                // dd($key);
                $id_number = $key['shopsId'];
                $clientOrder = new Client();
                $resOrder = $clientOrder->get("https://fs.tokopedia.net/v2/order/list?fs_id=$appID&shop_id=$id_number&from_date=$start_time&to_date=$current_time&page=1&per_page=50&status=220", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
                $orders = json_decode($resOrder);
                $ordersData = $orders->data;
                // dd($ordersData);
                

                if($ordersData != null) {
                    // satu order
                    foreach ($ordersData as $orderData) {
                        // decrypt code
                        $secret = base64_decode($orderData->encryption->secret);
                        $secretKey = $privateKey->decrypt($secret);
                        $bcontent = base64_decode($orderData->encryption->content);
                        $bnonce = substr($bcontent, strlen($bcontent) - 12, strlen($bcontent));
                        $bcipher = substr($bcontent, 0, strlen($bcontent) - 12);
    
                        // default tag
                        $taglength = 16;
                        $tag = substr($bcipher, strlen($bcipher) - $taglength, strlen($bcipher));
                        $acipher = substr($bcipher, 0, strlen($bcipher) - $taglength);
                        $result = openssl_decrypt(
                            $acipher, 
                            "aes-256-gcm",
                            $secretKey,
                            OPENSSL_RAW_DATA,
                            $bnonce,
                            $tag);
                        $decodeResult = json_decode($result);
                        $nmPembeli = $decodeResult->buyer->name;
                        $phone = $decodeResult->buyer->phone;
                        $address = $decodeResult->recipient->address->address_full;
                            $shopId = $orderData->shop_id;
                            $orderId = $orderData->order_id;
                            $invoice = $orderData->invoice_ref_num;
                            // get total qty and price
                            $qty = count($orderData->products);
                            $totalqty = 0;
                            $totalPaid = 0;
                            for($i = 0; $i<$qty; $i++){
                                $totalqty = $orderData->products[$i]->quantity + $totalqty;
                                $totalPaid = $orderData->products[$i]->total_price + $totalPaid;

                            }
                        // accept dulu semua 
                        //     // dd($totalqty, $totalPaid);
                        //     // $productId = $orderData->product_id;
                        //     // dd($orderId, $appID, $token->access_token);
                        //     $acceptOrder = $client->post("https://fs.tokopedia.net/v1/order/$orderId/fs/$appID/ack", ['headers' => [
                        //         'Authorization'=> "Bearer $token->access_token"
                        //     ]])->getBody()->getContents();
                        //     $decodeOrderAccepted = json_decode($acceptOrder);
                        //     $orderAccepted = $decodeOrderAccepted->data;
                        //      // dd($decodeOrderAccepted);
                      
                            
                        
                        // // array_push($tokpedOrders, $orderData->shop_id);
                        $kota = $orderData->recipient->address->city;
                        // $address = $orderData->recipient->address->address_full;
                        $invoice = $orderData->invoice_ref_num;
                        // // get product name
                        $products = $orderData->products;
                        // dd($invoice,$products,$address,$kota);

                        // ambil nama buyer
                        //cek nama buyer
                        $buyerSingleOrder = new Client();

                        $dataSingleOrder = $buyerSingleOrder->get("https://fs.tokopedia.net/v2/fs/$appID/order?invoice_num=$invoice", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
        
                        $nameOrder = json_decode($dataSingleOrder);
                        $nameOrderHistory = $nameOrder->data->order_info->order_history;
                        // $nama = '';
                        // foreach ($nameOrderHistory as $history) {
                        //     if($history->action_by== "buyer" || $history->hist_status_code == 100){
                        //     // dd($history->update_by);
                        //         $nama = $history->update_by;
                        //     };
                        // };
                        // cek di DB data sudah ada atau belum
                        $checkData = DB::table('bookingan')->where('orderId', $orderData->order_id)->first();
                        // dd($checkData);
                        // if($checkData==NULL  && $orderData->order_status == 220) {
                        if($checkData==NULL) {
                            // cek kurir
                            $kurir = $orderData->logistics->shipping_agency;
                            if($kurir == 'GoSend' || $kurir == 'GrabExpress' || $kurir == 'Ninja Xpress' || $kurir == 'GoSend' || $kurir == 'SiCepat'){
                                $logistic_type = 'pickUp';
                            }else{
                                $logistic_type = 'dropship';
                            }
                            
                            $toko = DB::table('Registered_store')->where('platform', 'Tokopedia')->where('shop_id', $shopId)->first();
                            $insert = DB::table('bookingan')->insertGetId(['toko'=>$toko->data_toko,'orderId' => $orderData->order_id, 'kode'=>$invoice, 'platform' => 'Tokopedia', 'nmToko' => $orderData->shop_id, 'created_at' => date('Y-m-d H:i:s'),'phone'=>$phone, 'kurir'=>$kurir, 'nama_pemesan'=>$nmPembeli, 'alamat'=>$address, 'jmlh_item'=>$totalqty, 'totalBayar'=>$totalPaid, 'logistic_type' => $logistic_type, 'order_status'=>'accept order']);

                            foreach ($products as $product) {
                                DB::table('order_from_marketplace_detail')->insert(['orderId'=>$insert, 'product_id'=>$product->id, 'item'=> $product->name, 'qty'=>$product->quantity, 'hargaSatuan'=>$product->price, 'hargaTotal'=>$product->total_price]);
                            }
                            
                            // download label
                              // if($orderAccepted == "success"){
                        //     // $changeStatus = DB::table('bookingan')->where('id', $insert)->update([
                        //     // 'status'=>'inactive',
                        //     // 'order_status'=>400,
                        //     // 'accept'=>'automatic'
                        //     // ]);
                            
                        //     $clientShipping = new Client();
                        //     $resShippingLabel = $clientShipping->get("https://fs.tokopedia.net/v1/order/$orderId/fs/$appID/shipping-label?printed=0", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
                        //     $shippingLabel = json_decode($resShippingLabel);
                        //     // dd($resShippingLabel)
                        //     $pdf = new \PDF();
                        //     $pdf = \PDF::loadView('admin.labelPdf', compact('resShippingLabel'));
                        //     $content = $pdf->download()->getOriginalContent();
                        //     \Storage::disk('local')->put("uploads/Tokopedia-$invoice.pdf",$content);
                        //     $path = "uploads/Tokopedia-$orderId.pdf";
                        //     $updateLinkResi = DB::table('bookingan')->where('id', $insert)->update([
                        //         'buktiBayar'=>$path
                        //     ]);
                        // }
                            // satu produk
                            $config['content'] = "Order Masuk di Tokopedia ".$namaToko;
                            $config['to'] = $key['to'];
                            $config['id_cms_users'] = $notifSendTo;
                            \CRUDBooster::sendNotification($config);
                        }
                    }
                    // return 'success data inserted';
                }else {
                    // return 'no data';
                }
            }
        }catch (RequestException $e) {
            // dd(Psr7\str($e->getRequest()));
            // dd($e);
            echo Psr7\str($e->getRequest());
                if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
                }
        }
        // dd($tokpedOrders);
        return 'success';
    }
    
    public function orderStatus(){

        

        $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");

        $appID = env('TOKOPEDIA_APP_ID');
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        $token = json_decode($res);
        
        $getAllorders = DB::table('order_from_marketplace')->where('platform', 'Tokopedia')->get();
        $allOrders = collect($getAllorders)->filter(function ($order) {return $order->order_status != '700';});
        // dd($allOrders);
        // get product by shops id
        foreach ($allOrders as $key => $value) {
            $invoice = $value->Invoice;
            // dd($value);
            $clientProduct = new Client();
            $resGetProducts = $clientProduct->get("https://fs.tokopedia.net/v2/fs/$appID/order?invoice_num=$invoice",['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
            $resGetProduct = json_decode($resGetProducts);
            $status = $resGetProduct->data->order_status;
            // dd($resGetProduct);
            
            // dd($statusString == $value->order_status);
            if($status != $value->order_status) {
                $updateOrderStatus = DB::table('order_from_marketplace')->where('Invoice', $invoice)->update([
                    'order_status'=>$status,
                ]);
                if($status == 450){
                    $updateOrderStatusBookingan = DB::table('bookingan')->where('kode', $invoice)->update([
                        'status'=>'barang siap dikirim',
                    ]);
                }
                elseif($status == 540){
                    $updateOrderStatusBookingan = DB::table('bookingan')->where('kode', $invoice)->update([
                        'status'=>'pesanan dikirim',
                    ]);
                }
                elseif($status == 600){
                    $updateOrderStatusBookingan = DB::table('bookingan')->where('kode', $invoice)->update([
                        'status'=>'pengiriman selesai',
                    ]);
                }
            }
        }
    
    }
    
    public function chatTokped(){

        $notifSendTo = DB::table('cms_users')->where(function ($query) {
            $query->where('id_cms_privileges', '=', 1)
                  ->orWhere('id_cms_privileges', '=', 2);
                })
                  ->pluck('id');

        $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");

        $appID = env('TOKOPEDIA_APP_ID');
        try{
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        }catch (RequestException $e) {
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        $token = json_decode($res);

        $shopsData = [
            'warisan Sriwijaya' => [
                'shopsId' => 10696883,
                'to' => "https://www.tokopedia.com/warisansriwijaya"
            ],
            'Warisasn Hayam Wuruk' => [
                'shopsId' => 9040202,
                'to' => "https://www.tokopedia.com/warisanhayam"
            ],
            'Warisan Gajahmada' => [
                'shopsId' => 8383447,
                'to' => "https://www.tokopedia.com/warisangajahmada"
            ],
            'Warisan Gajahmada Solo' => [
                'shopsId' => 9226747,
                'to' => "https://www.tokopedia.com/wgajahmadasolo"
            ],
            'Warisan Gajahmada Yogyakarta' => [
                'shopsId' => 9235772,
                'to' => "https://www.tokopedia.com/Wgmyogya"
            ],
            'Warisan Gajahmada Denpasar' => [
                'shopsId' => 9583529,
                'to' => "https://www.tokopedia.com/Warisangajahmadadenpasar"
            ],
            
        ];

        try{
            foreach ($shopsData as $namaToko=>$key) {
                // list Message
                date_default_timezone_set("Asia/Jakarta");
                $shop_Id = $key["shopsId"];
                $getChatListMessage = $client->get("https://fs.tokopedia.net/v1/chat/fs/$appID/messages?shop_id=$shop_Id&filter=unread", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
                $chatListMessage = json_decode($getChatListMessage);
                $listMessage = $chatListMessage->data;
                // $mil = 1618474661943;
                // $time = date("y-m-d H:i:s", ($mil/1000));
                // dd($listMessage);
                
                if($listMessage != null){
                    // dd('mauo');
                    foreach ($listMessage as $data) {
                        // insert list message
                        $customer = $data->attributes->contact->attributes->name;
                        $msg_id = $data->msg_id;
                        $msg = $data->attributes->last_reply_msg;
                        // cek dulu, kalau tidak ada baru masukkan
                        $ceklistMessage = DB::table('message_list')->where('msg_id', $msg_id)->first();
                        // dd($ceklistMessage);
                        if($ceklistMessage == null){
                            // dd('masuk');
                            $insert = DB::table('message_list')->insertGetId([
                            "shop_id"=>$shop_Id,
                            "nama_toko"=>$namaToko,
                            "platform"=>'tokopedia',
                            "sender_name"=>$customer,
                            "msg_id"=>$msg_id,
                            "msg"=>$msg,
                            "created_at"=>date('Y-m-d H:i:s')
                            // "last_reply_time"=>
                        ]);
                    
                        $config['content'] = "Chat dari".$customer."di Tokopedia dari toko".$namaToko." chat id=".$msg_id;
                            $config['to'] = $key['to'];
                            $config['id_cms_users'] = $notifSendTo;
                            \CRUDBooster::sendNotification($config);
                    
                        }
                    }
                }else {

                }
                
            }
            
        }catch (RequestException $e) {
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        return 'success';
    }
    
    public function chatTokped1(){

        $notifSendTo = DB::table('cms_users')->where(function ($query) {
            $query->where('id_cms_privileges', '=', 1)
                  ->orWhere('id_cms_privileges', '=', 2);
                })
                  ->pluck('id');

        $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");

        $appID = env('TOKOPEDIA_APP_ID');
        try{
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        }catch (RequestException $e) {
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        $token = json_decode($res);

        $shopsData = [
            'BARDI Banten Official'=>[
                'shopsId'=>11466905,
                'to'=>"https://www.tokopedia.com/bardibanten"
            ],
            'Warisasn Gajahmada Semarang' => [
                'shopsId' => 9212736,
                'to' => "https://www.tokopedia.com/warisangajahmadasemarang"
            ],
            'Warisan Gajahmada Cirebon' => [
                'shopsId' => 9585109,
                'to' => "https://www.tokopedia.com/Warisangajahmadacirebon"
            ],
            'Warisan Gajahmada Surabaya' => [
                'shopsId' => 9235772,
                'to' => "https://www.tokopedia.com/warisangajahmadasurabaya"
            ],
            
        ];

        try{
            foreach ($shopsData as $namaToko=>$key) {
                // list Message
                date_default_timezone_set("Asia/Jakarta");
                $shop_Id = $key["shopsId"];
                $getChatListMessage = $client->get("https://fs.tokopedia.net/v1/chat/fs/$appID/messages?shop_id=$shop_Id&filter=unread", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
                $chatListMessage = json_decode($getChatListMessage);
                $listMessage = $chatListMessage->data;
                // $mil = 1618474661943;
                // $time = date("y-m-d H:i:s", ($mil/1000));
                // dd($listMessage);
                
                if($listMessage != null){
                    // dd('mauo');
                    foreach ($listMessage as $data) {
                        // insert list message
                        $customer = $data->attributes->contact->attributes->name;
                        $msg_id = $data->msg_id;
                        $msg = $data->attributes->last_reply_msg;
                        // cek dulu, kalau tidak ada baru masukkan
                        $ceklistMessage = DB::table('message_list')->where('msg_id', $msg_id)->first();
                        // dd($ceklistMessage);
                        // dd($msg);
                        if($ceklistMessage == null){
                            // dd('masuk');
                            $insert = DB::table('message_list')->insertGetId([
                            "shop_id"=>$shop_Id,
                            "nama_toko"=>$namaToko,
                            "platform"=>'tokopedia',
                            "sender_name"=>$customer,
                            "msg_id"=>$msg_id,
                            "msg"=>$msg,
                            "created_at"=>date('Y-m-d H:i:s')
                            // "last_reply_time"=>
                        ]);
                    
                        $config['content'] = "Chat dari".$customer."di Tokopedia dari toko".$namaToko." chat id=".$msg_id;
                            $config['to'] = $key['to'];
                            $config['id_cms_users'] = $notifSendTo;
                            \CRUDBooster::sendNotification($config);
                    
                        }
                    }
                }else {

                }
                
            }
            
        }catch (RequestException $e) {
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        return 'success';
    }
    
    public function AutoAcceptOrder(){
        $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");

        $appID = env('TOKOPEDIA_APP_ID');
        try{
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        }catch (RequestException $e) {
            // dd(Psr7\str($e->getRequest()));
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        $token = json_decode($res);

        date_default_timezone_set("Asia/Jakarta");
        $date = new \DateTime;
        // $date->modify('-5 minutes');
        $formatted_date = $date->format('Y-m-d H:i:s');
        // dd($formatted_date);
        $getAllOrders = DB::table('order_from_marketplace_detail')->join('order_from_marketplace', 'order_from_marketplace_detail.orderId', 'order_from_marketplace.id')->where('order_from_marketplace.platform', 'Tokopedia')->where('order_from_marketplace.status', 'active')->where('order_from_marketplace.created_at', '<=', $formatted_date)->select('order_from_marketplace_detail.product_id', 'order_from_marketplace.orderId', 'order_from_marketplace.nmToko', 'order_from_marketplace.Kurir')->get();
        
        if($getAllOrders != null){
            try{
            foreach ($getAllOrders as $key => $value) {

                $orderId = $value->orderId;
                $shopId = $value->nmToko;
                $productId = $value->product_id;
                    // dd($orderId, $appID, $token->access_token);
                    $acceptOrder = $client->post("https://fs.tokopedia.net/v1/order/$orderId/fs/$appID/ack", ['headers' => [
                        'Authorization'=> "Bearer $token->access_token"
                        ]])->getBody()->getContents();
                    $decodeOrderAccepted = json_decode($acceptOrder);
                    $orderAccepted = $decodeOrderAccepted->data;
                    // dd($decodeOrderAccepted);
                    if($orderAccepted == "success"){
                        $changeStatus = DB::table('order_from_marketplace')->where('orderId', $orderId)->update([
                            'status'=>'inactive',
                            'order_status'=>400,
                            'accept'=>'automatic'
                            ]);
                        }
                        
                        $clientShipping = new Client();
                        $resShippingLabel = $clientShipping->get("https://fs.tokopedia.net/v1/order/$orderId/fs/$appID/shipping-label?printed=0", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
                    $shippingLabel = json_decode($resShippingLabel);
                    // dd($resShippingLabel)
                    $pdf = new \PDF();
                    $pdf = \PDF::loadView('admin.labelPdf', compact('resShippingLabel'));
                    $content = $pdf->download()->getOriginalContent();
                    \Storage::disk('local')->put("uploads/Tokopedia-$orderId.pdf",$content);
                    $path = "uploads/Tokopedia-$orderId.pdf";
                    $updateLinkResi = DB::table('order_from_marketplace')->where('orderId', $orderId)->update([
                        'shipping_label'=>$path
                    ]);
                    
                    // dd($orderAccepted);

            }
            
            }catch (RequestException $e) {
                // dd(Psr7\str($e->getRequest()));
                // dd($e);
                echo Psr7\str($e->getRequest());
                if ($e->hasResponse()) {
                    echo Psr7\str($e->getResponse());
                }
            }
        }
            echo 'success';
    }
    
    public function requestPickUp($id){
        $getProduct = DB::table('bookingan')->where('bookingan.id', $id)->join('order_from_marketplace', 'bookingan.kode', 'order_from_marketplace.Invoice')->select('order_from_marketplace.orderId', 'order_from_marketplace.nmToko')->first();
        $orderId = (int)$getProduct->orderId;
        // dd($orderId);
        $nmToko = (int)$getProduct->nmToko;
        $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");

        $appID = env('TOKOPEDIA_APP_ID');
        try{
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        }catch (RequestException $e) {
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        $token = json_decode($res);

        try{
            $requestPickUp = $client->post("https://fs.tokopedia.net/inventory/v1/fs/$appID/pick-up", ['headers' => [
            'Authorization'=> "Bearer $token->access_token",
            'Content-Type' => "application/json"
            ],
            'json'=> 
                [
                    "order_id"=>$orderId, 
                    "shop_id"=>$nmToko
                ]
            
            ])->getBody()->getContents();
            
            $decodeRequestPickUp = json_decode($requestPickUp);
        }catch (RequestException $e){
            // dd(Psr7\str($e->getRequest()));
            $error = (json_decode($e->getResponse()->getBody()->getContents()));
                dd($error);
                echo Psr7\str($e->getRequest());
                if ($e->hasResponse()) {
                    echo Psr7\str($e->getResponse());
                }
        }
        
        // dd($decodeRequestPickUp);
        $pickUpRequest = $decodeRequestPickUp->data;
        if($pickUpRequest != null){
            DB::table('bookingan')->where('id', $id)->update([
                'pick_up'=>null
            ]);
            
        }
        return redirect()->back();
    }
    
    public function cetakLabel($kode){
        $kode = decrypt($kode);
        $getOrderId = DB::table('order_from_marketplace')->where('Invoice', $kode)->select('orderId')->first();
        $orderId = $getOrderId->orderId;
         $appID = env('TOKOPEDIA_APP_ID');
        try{
            $client = new Client();
            $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        }catch (RequestException $e) {
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        $token = json_decode($res);
        // dd($token);
        $clientShipping = new Client();
        $resShippingLabel = $clientShipping->get("https://fs.tokopedia.net/v1/order/$orderId/fs/$appID/shipping-label?printed=0", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
        // dd($resShippingLabel);
        $shippingLabel = json_decode($resShippingLabel);
        $pdf = new \PDF();
        $pdf = \PDF::loadView('admin.labelPdf', compact('resShippingLabel'));
        return view('admin.labelPdf', compact('resShippingLabel'));
    }
    
}
