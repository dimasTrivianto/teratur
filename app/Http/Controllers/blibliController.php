<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

use function GuzzleHttp\json_decode;

class blibliController extends Controller
{
    //
    public function getOrder(){
        $notifSendTo = \DB::table('cms_users')->where(function ($query) {
            $query->where('id_cms_privileges', '=', 1)
                  ->orWhere('id_cms_privileges', '=', 2);
                })
                  ->pluck('id');
        // echo $notifSendTo;
        // dd('stop');
        // echo ($notifSendTo);
        // dd($notifSendTo);
        
        $sellerCode = [];
        // $sellerCode[0]['sellerCode'] = 'WAM-70000';
        // $sellerCode[0]['nmToko'] = 'Warisan Majapahit';
        // $sellerCode[0]['sellerApiKey'] = 'C9F7F44E8E5BD1B40E96456DD23F45A792E3EEAE60F12C7A73030ACBDB3A3DA7';
        // $sellerCode[1]['sellerCode'] = 'WAG-70003';
        // $sellerCode[1]['nmToko'] = 'Warisan Gajahmada JakUt';
        // $sellerCode[1]['sellerApiKey'] = ' ';
        // $sellerCode[2]['sellerCode'] = 'WAG-70004';
        // $sellerCode[2]['nmToko'] = 'Warisan Gajahmada JakSel';
        // $sellerCode[2]['sellerApiKey'] = ' ';
        // $sellerCode[3]['sellerCode'] = 'WAG-70012';
        // $sellerCode[3]['nmToko'] = 'Warisan Gajahmada Depok';
        // $sellerCode[3]['sellerApiKey'] = '39412629FF252A00D9D1C40F612CAECBD1DD8DD015FB011B815BDD34CFF8AC41';
        // $sellerCode[4]['sellerCode'] = 'WAG-70010';
        // $sellerCode[4]['nmToko'] = 'Warisan Gajahmada Bekasi';
        // $sellerCode[4]['sellerApiKey'] = ' ';
        // $sellerCode[5]['sellerCode'] = 'WAG-70013';
        // $sellerCode[5]['nmToko'] = 'Warisan Gajahmada Bogor';
        // $sellerCode[5]['sellerApiKey'] = ' ';
        // $sellerCode[6]['sellerCode'] = 'WAG-70011';
        // $sellerCode[6]['nmToko'] = 'Warisan Gajahmada Jakbar';
        // $sellerCode[6]['sellerApiKey'] = ' ';
        // $sellerCode[7]['sellerCode'] = 'WAG-70030';
        // $sellerCode[7]['nmToko'] = 'Warisan Gajahmada Surabaya';
        // $sellerCode[7]['sellerApiKey'] = '93B86D47253DA2D08013EE89BF3D1605E3F2797E7CCBC9CF039A198358CB1E7F';
        // $sellerCode[8]['sellerCode'] = 'WAG-70024';
        // $sellerCode[8]['nmToko'] = 'Warisan Gajahmada Cirebon';
        // $sellerCode[8]['sellerApiKey'] = '06A825311F5952C887427318A3E0AE60101B7F2FA76D3313B0D10E2A655F38ED';
        // $sellerCode[9]['sellerCode'] = 'WAG-70023';
        // $sellerCode[9]['nmToko'] = 'Warisan Gajahmada Jogjakarta';
        // $sellerCode[9]['sellerApiKey'] = '280E783B67C9B1667A3962C4D16FA729F65FE6ADBB455E971B75C56D450B2B65';
        // $sellerCode[10]['sellerCode'] = 'WAG-70021';
        // $sellerCode[10]['nmToko'] = 'Warisan Gajahmada Bali';
        // $sellerCode[10]['sellerApiKey'] = '1DD1633C4262CA0D78735132000ACBD93CDB90AE4DCDDA2DCB90339B1BDD268C';
        // $sellerCode[11]['sellerCode'] = 'WAG-60046';
        // $sellerCode[11]['nmToko'] = 'Warisan Gajahmada';
        // $sellerCode[11]['sellerApiKey'] = '3FE68EF43ED12FF6534024952D1E31E5210A5660989134025241136DA576ABE1';
        
        $sellerCode[0]['sellerCode'] = 'WAM-70000';
        $sellerCode[0]['nmToko'] = 'Warisan Majapahit';
        $sellerCode[0]['sellerApiKey'] = 'C9F7F44E8E5BD1B40E96456DD23F45A792E3EEAE60F12C7A73030ACBDB3A3DA7';
        $sellerCode[1]['sellerCode'] = 'WAG-70030';
        $sellerCode[1]['nmToko'] = 'Warisan Gajahmada Surabaya';
        $sellerCode[1]['sellerApiKey'] = '39412629FF252A00D9D1C40F612CAECBD1DD8DD015FB011B815BDD34CFF8AC41';
        //Error "errorCode":"ERR-MA403015 "errorMessage":"Seller's postlive status doesn't fulfill the criteria to access the data"
        $sellerCode[2]['sellerCode'] = 'WAG-70024'; 
        $sellerCode[2]['nmToko'] = 'Warisan Gajahmada Cirebon';
        $sellerCode[2]['sellerApiKey'] = '06A825311F5952C887427318A3E0AE60101B7F2FA76D3313B0D10E2A655F38ED';
        //EndError
        $sellerCode[10]['sellerCode'] = 'WAG-70023';
        $sellerCode[10]['nmToko'] = 'Warisan Gajahmada Jogjakarta';
        $sellerCode[10]['sellerApiKey'] = '280E783B67C9B1667A3962C4D16FA729F65FE6ADBB455E971B75C56D450B2B65';
        $sellerCode[3]['sellerCode'] = 'WAG-70021';
        $sellerCode[3]['nmToko'] = 'Warisan Gajahmada Bali';
        $sellerCode[3]['sellerApiKey'] = '1DD1633C4262CA0D78735132000ACBD93CDB90AE4DCDDA2DCB90339B1BDD268C';
        $sellerCode[4]['sellerCode'] = 'WAG-60046';
        $sellerCode[4]['nmToko'] = 'Warisan Gajahmada';
        $sellerCode[4]['sellerApiKey'] = '3FE68EF43ED12FF6534024952D1E31E5210A5660989134025241136DA576ABE1';
        //Data baru
        $sellerCode[5]['nmToko'] = 'Warisan Gajahmada Bogor';
        $sellerCode[5]['sellerApiKey'] = '39412629FF252A00D9D1C40F612CAECBD1DD8DD015FB011B815BDD34CFF8AC41';
        $sellerCode[5]['sellerCode'] = 'WAG-70013';
        $sellerCode[6]['nmToko'] = 'Warisan Gajahmada Bekasi';
        $sellerCode[6]['sellerApiKey'] = '39412629FF252A00D9D1C40F612CAECBD1DD8DD015FB011B815BDD34CFF8AC41';
        $sellerCode[6]['sellerCode'] = 'WAG-70010';
        $sellerCode[7]['sellerCode'] = 'WAG-70004';
        $sellerCode[7]['nmToko'] = 'Warisan Gajahmada JakSel';
        $sellerCode[7]['sellerApiKey'] = '39412629FF252A00D9D1C40F612CAECBD1DD8DD015FB011B815BDD34CFF8AC41';
        $sellerCode[8]['sellerCode'] = 'WAG-70003';
        $sellerCode[8]['nmToko'] = 'Warisan Gajahmada JakUt';
        $sellerCode[8]['sellerApiKey'] = '39412629FF252A00D9D1C40F612CAECBD1DD8DD015FB011B815BDD34CFF8AC41';
        $sellerCode[9]['sellerCode'] = 'WAG-70011';
        $sellerCode[9]['nmToko'] = 'Warisan Gajahmada Jakbar';
        $sellerCode[9]['sellerApiKey'] = '39412629FF252A00D9D1C40F612CAECBD1DD8DD015FB011B815BDD34CFF8AC41';
        $sellerCode[11]['sellerCode'] = 'WAG-70012';
        $sellerCode[11]['nmToko'] = 'Warisan Gajahmada Depok';
        $sellerCode[11]['sellerApiKey'] = '39412629FF252A00D9D1C40F612CAECBD1DD8DD015FB011B815BDD34CFF8AC41';
        //WHW
        $sellerCode[12]['sellerCode'] = 'WAH-60054';
        $sellerCode[12]['nmToko'] = 'Warisan Hayam Wuruk Bali';
        $sellerCode[12]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';
       
        $sellerCode[13]['sellerCode'] = 'WAH-60053';
        $sellerCode[13]['nmToko'] = 'Warisan Hayam Wuruk Surakarta';
        $sellerCode[13]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';
       
        $sellerCode[14]['sellerCode'] = 'WAH-60057';
        $sellerCode[14]['nmToko'] = 'Warisan Hayam Wuruk Bandung';
        $sellerCode[14]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';
        $sellerCode[15]['sellerCode'] = 'WAH-60050';
        $sellerCode[15]['nmToko'] = 'Warisan Hayam Wuruk Yogyakarta';
        $sellerCode[15]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';
        $sellerCode[16]['sellerCode'] = 'WAH-60055';
        $sellerCode[16]['nmToko'] = 'Warisan Hayam Wuruk Semarang';
        $sellerCode[16]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';
        $sellerCode[17]['sellerCode'] = 'WAH-60051';
        $sellerCode[17]['nmToko'] = 'Warisan Hayam Wuruk Surabaya';
        $sellerCode[17]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';
        $sellerCode[18]['sellerCode'] = 'WAH-60049';
        $sellerCode[18]['nmToko'] = 'Warisan Hayam Wuruk Pusat';
        $sellerCode[18]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';
        $sellerCode[19]['sellerCode'] = 'WAH-60052';
        $sellerCode[19]['nmToko'] = 'Warisan Hayam Wuruk Malang';
        $sellerCode[19]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';
        $sellerCode[20]['sellerCode'] = 'WAH-60056';
        $sellerCode[20]['nmToko'] = 'Warisan Hayam Wuruk Tasikmalaya';
        $sellerCode[20]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';
        $sellerCode[21]['sellerCode'] = 'WAH-60058';
        $sellerCode[21]['nmToko'] = 'Warisan Hayam Wuruk Bogor';
        $sellerCode[21]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';
      
        $blibliOrders = [];
        $client = new Client();
        $clientId = env('BLIBLI_CLIENT_ID');
        $clientKey = env('BLIBLI_CLIENT_KEY');
        $Authorization = base64_encode("{$clientId}:{$clientKey}");
        try {
        foreach($sellerCode as $seller){
            $res = $client->get('https://api.blibli.com/v2/proxy/mta/api/businesspartner/v1/order/orderList?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&businessPartnerCode='.$seller['sellerCode'].'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001', ['headers' => [
                'Authorization'=> "Basic $Authorization",
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Content-Length' => 0,
                'User_Agent' => 'PostmanRuntime/7.17.1',
                'Api-Seller-Key' => $seller['sellerApiKey']
                ]])->getBody();
            $res = $res->getContents();
            $response = json_decode($res);
            $datas = $response->content;
            // dd($response->content);
            // dd($response);
            // dd($datas);
            if($response->content != null) {
                foreach($datas as $data){
                    // dd($data);
                    // dd($data->orderItemNo, $data->orderNo);
                    // dd($data->customerFullName);
                    //Get each order detail
                    // dd('cek');
                   
                    //End get each order detail
                    
                    //Cek di db ada atau belum
                    $checkData = \DB::table('bookingan')->where('orderId',$data->orderNo)->first();
                    // dd($checkData);
                    // dd($checkData);
                    if($checkData==NULL && $data->orderStatus=='FP'){
                     //If belum ada dan pesanan baru
                    // dd($data->customerFullName);
                    $getDetail = $client->get('https://api.blibli.com/v2/proxy/mta/api/businesspartner/v1/order/orderDetail?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeId=10001&orderNo='.$data->orderNo.'&orderItemNo='.$data->orderItemNo.'&channelId=PT. YOKESEN TEKNOLOGI', ['headers' => [
                        'Authorization'=> "Basic $Authorization",
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        'Content-Length' => 0,
                        'User_Agent' => 'PostmanRuntime/7.17.1',
                        'Api-Seller-Key' => $seller['sellerApiKey']
                        ]])->getBody();
                    // dd($getDetail);
                    $getDetail = $getDetail->getContents();
                    // dd($getDetail);
                    $responseDetail = json_decode($getDetail);
                    $detailOrder = $responseDetail->value;
                    // dd($detailOrder);
                    
                    // shipping label
                    $packageId = $detailOrder->packageId;
                    $storeId = $detailOrder->storeId;
                    $sellerCode = $seller['sellerCode'];
                    $custName = $detailOrder->custName;
                    $getShippingLabel = $client->get("https://api.blibli.com/v2/proxy/seller/v1/orders/$packageId/shippingLabel?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&username=daniel.yokesen@gmail.com&storeId=$storeId&storeCode=$sellerCode&channelId=PT. YOKESEN TEKNOLOGI", ['headers' => [
                        'Authorization'=> "Basic $Authorization",
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        'Content-Length' => 0,
                        'User_Agent' => 'PostmanRuntime/7.17.1',
                        'Api-Seller-Key' => $seller['sellerApiKey']
                        ]])->getBody();
                    $getShippingLabel = $getShippingLabel->getContents();
                    $responseShippingLabel = json_decode($getShippingLabel);
                    $shippingLabel = $responseShippingLabel->content->shippingLabel;
                    date_default_timezone_set("Asia/Jakarta");
                    $file = base64_decode($shippingLabel);
                    $uniqueFileName = str_replace(" ","",time()."-".$custName.'.pdf');
                    \Storage::disk('local')->put('uploads/'.$uniqueFileName, $file);
                    $filePath = 'uploads/'.$uniqueFileName;
                    
                    // return 'masuk if atas';
                    $insertId = \DB::table('bookingan')->insertGetId([
                        'orderId' => $data->orderNo, 
                        'platform' => 'Blibli',
                        'nmToko' => $seller['sellerCode'],
                        'created_at' => date('Y-m-d H:i:s'),
                        'nmPembeli' => $data->customerFullName,
                        'kota' => $detailOrder->shippingCity,
                        'alamat' => $detailOrder->shippingStreetAddress,
                        'kurir' => $data->logisticsProductName,
                        'order_status'=>$data->orderStatus,
                        'noHp'=>$detailOrder->shippingMobile,
                        'item_no'=>$detailOrder->orderItemNo,
                        'shipping_label' => $filePath,
                        'Invoice'=>$detailOrder->orderNo,
                        'status' => 'active']);
                    // dd($insertId);
                    foreach($datas as $dt){
                        if($data->orderNo == $dt->orderNo){
                            $insertDetail =   \DB::table('order_from_marketplace_detail')->insert([
                                'orderId' => $insertId, 
                                'item' => $dt->productName,
                                'qty' => $dt->qty,
                                'product_id'=>$dt->itemSku
                                ]); 
                        }
                    }
                    
                    $config['content'] = "Order Masuk di Blibli ".$seller['nmToko'];
                    $config['to'] = '#';
                    // $config['to'] = $seller['nmToko'];
                    $config['id_cms_users'] = $notifSendTo;
                    \CRUDBooster::sendNotification($config);
                    
                    // array_push($blibliOrders, $response->content);
                    }
                    else if($checkData!=NULL && $data->orderStatus != 'FP'){
                        // return 'masuk elseif';
                        // dd($data);
                        \DB::table('order_from_marketplace')
                            ->where('orderId', $data->orderNo)
                            ->update(['status' => 'inactive']);
                            // return 'masuk elseif';
                    }
                    // return 'tidak masuk keduanya';
                    
                    
                }
            }
        }
        }
        catch (\GuzzleHttp\Exception\ClientException $e) {
                // dd('masuk sini');
                // dd($e->getResponse());
                // dd($e->getResponse()->getBody()->getContents());

            }
        
        
        // $data = json_decode($res);
        
        // $response = $response['content'];
        
        // $data = $data->content;
        // $data = $data->toJson();
        // dd($blibliOrders);
        return 'Success';
    }
    
    public function orderStatusBlibli(){
        $sellerCode = [];
        
        $sellerCode[0]['sellerCode'] = 'WAM-70000';
        $sellerCode[0]['nmToko'] = 'Warisan Majapahit';
        $sellerCode[0]['sellerApiKey'] = 'C9F7F44E8E5BD1B40E96456DD23F45A792E3EEAE60F12C7A73030ACBDB3A3DA7';
        $sellerCode[1]['sellerCode'] = 'WAG-70030';
        $sellerCode[1]['nmToko'] = 'Warisan Gajahmada Surabaya';
        $sellerCode[1]['sellerApiKey'] = '39412629FF252A00D9D1C40F612CAECBD1DD8DD015FB011B815BDD34CFF8AC41';
        //Error "errorCode":"ERR-MA403015 "errorMessage":"Seller's postlive status doesn't fulfill the criteria to access the data"
        $sellerCode[2]['sellerCode'] = 'WAG-70024'; 
        $sellerCode[2]['nmToko'] = 'Warisan Gajahmada Cirebon';
        $sellerCode[2]['sellerApiKey'] = '06A825311F5952C887427318A3E0AE60101B7F2FA76D3313B0D10E2A655F38ED';
        //EndError
        $sellerCode[10]['sellerCode'] = 'WAG-70023';
        $sellerCode[10]['nmToko'] = 'Warisan Gajahmada Jogjakarta';
        $sellerCode[10]['sellerApiKey'] = '280E783B67C9B1667A3962C4D16FA729F65FE6ADBB455E971B75C56D450B2B65';
        $sellerCode[3]['sellerCode'] = 'WAG-70021';
        $sellerCode[3]['nmToko'] = 'Warisan Gajahmada Bali';
        $sellerCode[3]['sellerApiKey'] = '1DD1633C4262CA0D78735132000ACBD93CDB90AE4DCDDA2DCB90339B1BDD268C';
        $sellerCode[4]['sellerCode'] = 'WAG-60046';
        $sellerCode[4]['nmToko'] = 'Warisan Gajahmada';
        $sellerCode[4]['sellerApiKey'] = '3FE68EF43ED12FF6534024952D1E31E5210A5660989134025241136DA576ABE1';
        //Data baru
        $sellerCode[5]['nmToko'] = 'Warisan Gajahmada Bogor';
        $sellerCode[5]['sellerApiKey'] = '39412629FF252A00D9D1C40F612CAECBD1DD8DD015FB011B815BDD34CFF8AC41';
        $sellerCode[5]['sellerCode'] = 'WAG-70013';
        $sellerCode[6]['nmToko'] = 'Warisan Gajahmada Bekasi';
        $sellerCode[6]['sellerApiKey'] = '39412629FF252A00D9D1C40F612CAECBD1DD8DD015FB011B815BDD34CFF8AC41';
        $sellerCode[6]['sellerCode'] = 'WAG-70010';
        $sellerCode[7]['sellerCode'] = 'WAG-70004';
        $sellerCode[7]['nmToko'] = 'Warisan Gajahmada JakSel';
        $sellerCode[7]['sellerApiKey'] = '39412629FF252A00D9D1C40F612CAECBD1DD8DD015FB011B815BDD34CFF8AC41';
        $sellerCode[8]['sellerCode'] = 'WAG-70003';
        $sellerCode[8]['nmToko'] = 'Warisan Gajahmada JakUt';
        $sellerCode[8]['sellerApiKey'] = '39412629FF252A00D9D1C40F612CAECBD1DD8DD015FB011B815BDD34CFF8AC41';
        $sellerCode[9]['sellerCode'] = 'WAG-70011';
        $sellerCode[9]['nmToko'] = 'Warisan Gajahmada Jakbar';
        $sellerCode[9]['sellerApiKey'] = '39412629FF252A00D9D1C40F612CAECBD1DD8DD015FB011B815BDD34CFF8AC41';
        $sellerCode[11]['sellerCode'] = 'WAG-70012';
        $sellerCode[11]['nmToko'] = 'Warisan Gajahmada Depok';
        $sellerCode[11]['sellerApiKey'] = '39412629FF252A00D9D1C40F612CAECBD1DD8DD015FB011B815BDD34CFF8AC41';
        //WHW
        $sellerCode[12]['sellerCode'] = 'WAH-60054';
        $sellerCode[12]['nmToko'] = 'Warisan Hayam Wuruk Bali';
        $sellerCode[12]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';
       
        $sellerCode[13]['sellerCode'] = 'WAH-60053';
        $sellerCode[13]['nmToko'] = 'Warisan Hayam Wuruk Surakarta';
        $sellerCode[13]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';
       
        $sellerCode[14]['sellerCode'] = 'WAH-60057';
        $sellerCode[14]['nmToko'] = 'Warisan Hayam Wuruk Bandung';
        $sellerCode[14]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';
        $sellerCode[15]['sellerCode'] = 'WAH-60050';
        $sellerCode[15]['nmToko'] = 'Warisan Hayam Wuruk Yogyakarta';
        $sellerCode[15]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';
        $sellerCode[16]['sellerCode'] = 'WAH-60055';
        $sellerCode[16]['nmToko'] = 'Warisan Hayam Wuruk Semarang';
        $sellerCode[16]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';
        $sellerCode[17]['sellerCode'] = 'WAH-60051';
        $sellerCode[17]['nmToko'] = 'Warisan Hayam Wuruk Surabaya';
        $sellerCode[17]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';
        $sellerCode[18]['sellerCode'] = 'WAH-60049';
        $sellerCode[18]['nmToko'] = 'Warisan Hayam Wuruk Pusat';
        $sellerCode[18]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';
        $sellerCode[19]['sellerCode'] = 'WAH-60052';
        $sellerCode[19]['nmToko'] = 'Warisan Hayam Wuruk Malang';
        $sellerCode[19]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';
        $sellerCode[20]['sellerCode'] = 'WAH-60056';
        $sellerCode[20]['nmToko'] = 'Warisan Hayam Wuruk Tasikmalaya';
        $sellerCode[20]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';
        $sellerCode[21]['sellerCode'] = 'WAH-60058';
        $sellerCode[21]['nmToko'] = 'Warisan Hayam Wuruk Bogor';
        $sellerCode[21]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';
      
        $blibliOrders = [];
        $client = new Client();
        $clientId = env('BLIBLI_CLIENT_ID');
        $clientKey = env('BLIBLI_CLIENT_KEY');
        $Authorization = base64_encode("{$clientId}:{$clientKey}");

        $getAllOrders = \DB::table('order_from_marketplace')->where('platform', 'Blibli')->get();
        $allOrders = collect($getAllOrders)->filter(function ($order) {return $order->order_status != 'D';});
        // dd($allOrders);
        
        foreach ($allOrders as $key => $value) {
            // dd($value);
            $getDetail = $client->get('https://api.blibli.com/v2/proxy/mta/api/businesspartner/v1/order/orderDetail?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeId=10001&orderNo='.$value->orderId.'&orderItemNo='.$value->item_no.'&channelId=PT. YOKESEN TEKNOLOGI', ['headers' => [
                'Authorization'=> "Basic $Authorization",
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Content-Length' => 0,
                'User_Agent' => 'PostmanRuntime/7.17.1',
                'Api-Seller-Key' => $seller['sellerApiKey']
                ]])->getBody();
            // dd($getDetail);
            $getDetail = $getDetail->getContents();
            $responseDetail = json_decode($getDetail);
            $detailOrder = $responseDetail->value;
            $status = $detailOrder->orderStatus;
            // dd($getDetail);
            if($value->order_status != $status){
                $updateOrderStatus = DB::table('order_from_marketplace')->where('Invoice', $invoice)->update([
                    'order_status'=>$status,
                ]);
                if($status == 'PU'){
                    $updateOrderStatusBookingan = DB::table('bookingan')->where('kode', $invoice)->update([
                        'status'=>'barang siap dikirim',
                    ]);
                }
                elseif($status == 'OS'){
                    $updateOrderStatusBookingan = DB::table('bookingan')->where('kode', $invoice)->update([
                        'status'=>'pesanan dikirim',
                    ]);
                }
                elseif($status == 'PU'){
                    $updateOrderStatusBookingan = DB::table('bookingan')->where('kode', $invoice)->update([
                        'status'=>'pengiriman selesai',
                    ]);
                }
                
            }
        }
    }
}
