<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
// use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Client;
use Carbon\Carbon;
use DB;
use hash_hmac;

class shopeeController extends Controller
{
    public function createAuth($id){
        $timestamp = time();
        $host = "https://partner.shopeemobile.com";
        // $host = "https://partner.test-stable.shopeemobile.com";
        $path = "/api/v2/shop/auth_partner";
        $redirect_url = "https://gudang.warisangajahmada.com/afterRedirect?id=$id";
        $partner_id = (int)env('SHOPEE_PARTNER_ID');
        $partner_key = env('SHOPEE_PARTNER_KEY');
        $salt = $partner_id.$path.$timestamp;
        $sign = hash_hmac('sha256', $salt, $partner_key);
        // generate api
        $url = $host.$path.'?partner_id='.$partner_id.'&timestamp='.$timestamp.'&sign='.$sign.'&redirect='.$redirect_url;
        // dd($url, $timestamp);
        return \Redirect::to($url);

    }
     public function createChatAuth($id){
        $timestamp = time();
        $host = "https://partner.shopeemobile.com";
        // $host = "https://partner.test-stable.shopeemobile.com";
        $path = "/api/v2/shop/auth_partner";
        $redirect_url = "https://gudang.warisangajahmada.com/afterRedirectChat?id=$id";
        $partner_id = (int)env('SHOPEE_PARTNER_CHAT_ID');
        $partner_key = env('SHOPEE_PARTNER_CHAT_KEY');
        $salt = $partner_id.$path.$timestamp;
        $sign = hash_hmac('sha256', $salt, $partner_key);
        // generate api
        $url = $host.$path.'?partner_id='.$partner_id.'&timestamp='.$timestamp.'&sign='.$sign.'&redirect='.$redirect_url;
        // dd($url, $timestamp);
        return \Redirect::to($url);

    }
    
    
    public function afterRedirect(Request $request){
        // create access token
        $code = $request->query('code');
        $shopId= $request->query('shop_id');
        $id= $request->query('id');
        $timestamp = time();
        $path = "/api/v2/auth/token/get";
        $partner_id = (int)env('SHOPEE_PARTNER_ID');
        $partner_key = env('SHOPEE_PARTNER_KEY');
        $salt = $partner_id.$path.$timestamp;
        $sign = hash_hmac('sha256', $salt, $partner_key);
        $expiredDate=date('Y-m-d', strtotime('+1 year'));
        $currentTime = Carbon::now()->format('Y-m-d H:i:s');
        $indonesiaTimeZone = Carbon::createFromFormat('Y-m-d H:i:s', $currentTime, 'UTC')
        ->setTimezone('Asia/Jakarta');
        try{
            // create token
                $client = new Client();
            $res = $client->post("https://partner.shopeemobile.com/api/v2/auth/token/get?sign=$sign&partner_id=$partner_id&timestamp=$timestamp", ['headers' => [
                'Content-Type' => 'application/json',
            ],
            'json'=>[
                    'code'=>$code,
                    // 'main_account_id'=>2001274,
                    'shop_id'=>(int)$shopId,
                    'partner_id'=>$partner_id
                ]
            ])->getBody()->getContents();
            $response = json_decode($res);
            $access_token = $response->access_token;
            $refresh_token = $response->refresh_token;
            // dd($response, $code, $shopId, $id, $refresh_token, $access_token, $expiredDate, $indonesiaTimeZone);
            $update = DB::table('Registered_store')->where('id', $id)->update([
                    'shop_id'=>$shopId,
                    'access_token'=>$access_token,
                    'refresh_token'=>$refresh_token,
                    'expired'=>$expiredDate,
                    'access_token_created'=>$indonesiaTimeZone,
                    'expired_in_second'=>$response->expire_in,
                    'shop_status'=>'active'
                ]);
            return \Redirect::to("https://gudang.warisangajahmada.com/");
        }catch (RequestException $e){
            dd($e);
        } 
    }
    public function afterRedirectChat(Request $request){
        // create access token
        $code = $request->query('code');
        $shopId= $request->query('shop_id');
        $id= $request->query('id');
        $timestamp = time();
        $path = "/api/v2/auth/token/get";
        $partner_id = (int)env('SHOPEE_PARTNER_CHAT_ID');
        $partner_key = env('SHOPEE_PARTNER_CHAT_KEY');
        $salt = $partner_id.$path.$timestamp;
        $sign = hash_hmac('sha256', $salt, $partner_key);
        $expiredDate=date('Y-m-d', strtotime('+1 year'));
        $currentTime = Carbon::now()->format('Y-m-d H:i:s');
        $indonesiaTimeZone = Carbon::createFromFormat('Y-m-d H:i:s', $currentTime, 'UTC')
        ->setTimezone('Asia/Jakarta');
        try{
            // create token
                $client = new Client();
            $res = $client->post("https://partner.shopeemobile.com/api/v2/auth/token/get?sign=$sign&partner_id=$partner_id&timestamp=$timestamp", ['headers' => [
                'Content-Type' => 'application/json',
            ],
            'json'=>[
                    'code'=>$code,
                    // 'main_account_id'=>2001274,
                    'shop_id'=>(int)$shopId,
                    'partner_id'=>$partner_id
                ]
            ])->getBody()->getContents();
            $response = json_decode($res);
            $access_token = $response->access_token;
            $refresh_token = $response->refresh_token;
            // dd($response, $code, $shopId, $id, $refresh_token, $access_token, $expiredDate, $indonesiaTimeZone);
            $update = DB::table('Registered_store')->where('id', $id)->update([
                    'shop_id'=>$shopId,
                    'chat_access_token'=>$access_token,
                    'chat_refresh_token'=>$refresh_token,
                    'chat_expired'=>$expiredDate,
                    'chat_access_token_created'=>$indonesiaTimeZone,
                    'chat_expired_in_second'=>$response->expire_in,
                    'shop_status'=>'active'
                ]);
            return \Redirect::to("https://gudang.warisangajahmada.com/");
        }catch (RequestException $e){
            dd($e);
        } 
    }
    
    public function getShopeeOrder(){
            
            $shops = DB::table('Registered_store')->where('status', 'active')->where('platform', 'Shopee')->get();
            // dd($shops_id);
            $timestamp = time();
            $partner_key = env('SHOPEE_PARTNER_KEY');
            $time_to = Carbon::now()->timestamp + 86400;
            $start_time = Carbon::now()->timestamp - 86400;
            $partner_id = (int)env('SHOPEE_PARTNER_ID');
            $path = "/api/v2/order/get_order_list";
            // dd($shops);
            // https://partner.shopeemobile.com/api/v2/auth/token/get
            try{
                foreach($shops as $value){
                    $shop_id = $value->shop_id;
                    $access_token = $value->access_token;
                    // cek if access token is valid or not
                    $access_expired_time = Carbon::parse($value->access_token_created)->addSecond($value->expired_in_second)->format('Y-m-d H:i:s');
                    $currentHour =  Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s');
                    $currentHourTimestamp = strtotime($currentHour);
                    $access_expired_time = strtotime($access_expired_time);
                    // dd($access_expired_time, $currentHourTimestamp > $access_expired_time, $currentHourTimestamp);
                    // jika token expired maka buat baru
                    // if($currentHourTimestamp > $access_expired_time){
                    //     $refresh_token = $value->refresh_token;
                    //     $path_refresh_token = "/api/v2/auth/access_token/get"; 
                    //     $salt_refresh = $partner_id.$path_refresh_token.$timestamp;
                    //     $sign_refresh = hash_hmac('sha256', $salt_refresh, $partner_key);
                    //     $clientRefresh = new Client();
                    //     // dd('tes');
                    //     $resRefresh = $clientRefresh->post("https://partner.shopeemobile.com/api/v2/auth/access_token/get?partner_id=$partner_id&timestamp=$timestamp&sign=$sign_refresh", ["headers"=>[
                    //             'Content-Type' => 'application/json',
                    //         ],
                    //         'json'=> [
                    //                 'shop_id'=>$shop_id,
                    //                 'refresh_token'=>$refresh_token,
                    //                 'partner_id'=>$partner_id
                    //             ]
                    //         ])->getBody()->getContents();
                    //     $response = json_decode($resRefresh);
                    //     $access_token = $response->access_token; 
                    //     $refresh_token = $response->refresh_token; 
                    //     $expired_in = $response->expire_in;
                    //     DB::table('Registered_store')->where('shop_id', $shop_id)->update([
                    //             'access_token'=>$access_token,
                    //             'refresh_token'=>$refresh_token,
                    //             'expired_in_second'=>$expired_in,
                    //             'access_token_created'=>$currentHour,
                    //         ]);
                    // }
                    // pakai shipment bukan order list
                    // $path = "/api/v2/order/get_order_list";
                    $path = "/api/v2/order/get_shipment_list";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $clientGetOrder = new Client();
                    // $resGetOrder = $clientGetOrder->get("https://partner.shopeemobile.com/api/v2/order/get_order_list?time_range_field=create_time&time_from=$start_time&time_to=$time_to&page_size=20&order_status=READY_TO_SHIP&response_optional_fields=order_status&shop_id=$shop_id&partner_id=$partner_id&access_token=$access_token&sign=$sign&timestamp=$timestamp")->getBody()->getContents();
                    $resGetOrder = $clientGetOrder->get("https://partner.shopeemobile.com/api/v2/order/get_shipment_list?page_size=20&shop_id=$shop_id&partner_id=$partner_id&access_token=$access_token&sign=$sign&timestamp=$timestamp")->getBody()->getContents();
                    $getOrder = json_decode($resGetOrder);
                    $order = $getOrder->response->order_list;
                    //  dd($getOrder, count($order));
                    if(count($order)>=1){
                        foreach($order as $key=>$values){
                            $order_sn=$values->order_sn;
                             $check = DB::table('bookingan')->where('kode', $order_sn)->first();
                             if($check==null){
                            //  dd($check, $invoice, $order_sn);
                                 
                                 $path = "/api/v2/order/get_order_detail";
                                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                                $sign = hash_hmac('sha256', $salt, $partner_key);
                                $clientOrderList = new Client();
                                $resOrderList = $clientOrderList->get("https://partner.shopeemobile.com/api/v2/order/get_order_detail?order_sn_list=$order_sn&response_optional_fields=total_amount,buyer_username,recipient_address,item_list,shipping_carrier,dropshipper,dropshipper_phone,invoice_data,package_list&shop_id=$shop_id&partner_id=$partner_id&access_token=$access_token&sign=$sign&timestamp=$timestamp")->getBody()->getContents();
                            
                                $responseList = json_decode($resOrderList);
        
                                $invoice = $responseList->response->order_list[0]->order_sn;
                                
                                $package_number = $responseList->response->order_list[0]->package_list[0]->package_number;
                                // $item = $responseList->response->order_list[0]->item_list[$key];
                                $orderItemQty =$responseList->response->order_list[0]->item_list;
                                // dd($orderItemQty, $responseList);
                                $insertId = DB::table('bookingan')->insertGetId([
                                    // 'orderId'=>$invoice,
                                    'kode'=>$invoice,
                                    'Platform'=>"Shopee",
                                    'nmToko'=>$shop_id,
                                    'nama_pemesan'=>$responseList->response->order_list[0]->recipient_address->name,
                                    'alamat'=>$responseList->response->order_list[0]->recipient_address->full_address,
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'order_status'=>'accept order',
                                    'kurir'=>$responseList->response->order_list[0]->shipping_carrier,
                                    'phone'=>$responseList->response->order_list[0]->recipient_address->phone,
                                    'toko'=>$value->toko,
                                    'package_id'=> $package_number
                                ]);
                                $qty = 0;
                                $totalPrice = 0;
                                // insert details
                                    foreach($orderItemQty as $index=>$item){
                                        $totalPrice = $item->model_original_price * $item->model_quantity_purchased;
                                        $insertItem = DB::table('order_from_marketplace_detail')->insert([
                                            'orderId'=>$insertId,
                                            'product_id'=>$item->item_id,
                                            'item'=>$item->item_name,
                                            'qty'=>$item->model_quantity_purchased,
                                            'hargaSatuan'=>$item->model_original_price,
                                            'hargaTotal'=>$totalPrice
                                        ]) ;
                                    $qty = $qty + $item->model_quantity_purchased;
                                    $totalPrice = $totalPrice + $totalPrice;
                                    }  
                                // update qty dan total
                                $update = DB::table('bookingan')->where('id', $insertId)->update([
                                        'jmlh_item'=>$qty,
                                        '$totalBayar'=>$totalPrice
                                    ]);
                             }else {
                                 echo 'data sudah ada';
                             }
                         
                        }
                    }else{
                    // dd('tidak');
                        
                    }
                    
                }
            }catch (RequestException $e){
                echo $e;
            }
            
            
          echo 'sukses';
        
    }
    
    public function shopeeChat(){
        $shops = DB::table('Registered_store')->where('status', 'active')->where('platform', 'Shopee')->get();
        foreach($shops as $shop){
            $notifSendTo = DB::table('cms_users')->where(function ($query) {
                        $query->where('id_cms_privileges', '=', 1)
                                ->orWhere('id_cms_privileges', '=', 2);
                            })
                                ->pluck('id');
            // unread count
            try{
                $shop_id = $shop->shop_id;
                $access_token = $shop->chat_access_token;
                $partner_id = (int)env('SHOPEE_PARTNER_CHAT_ID');
                $partner_key = env('SHOPEE_PARTNER_CHAT_KEY');
            // cek if access token is valid or not
                $access_expired_time = Carbon::parse($shop->chat_access_token_created)->addSecond($shop->chat_expired_in_second)->format('Y-m-d H:i:s');
                $currentHour =  Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s');
                $currentHourTimestamp = strtotime($currentHour);
                $access_expired_time = strtotime($access_expired_time);
                // dd($access_expired_time, $currentHour>$access_expired_time, $currentHour);
                if($currentHourTimestamp>$access_expired_time){
                    $refresh_token = $shop->chat_refresh_token;
                    $path_refresh_token = "/api/v2/auth/access_token/get"; 
                    $salt_refresh = $partner_id.$path_refresh_token.$timestamp;
                    $sign_refresh = hash_hmac('sha256', $salt_refresh, $partner_key);
                    $clientRefresh = new Client();
                    $resRefresh = $clientRefresh->post("https://partner.shopeemobile.com/api/v2/auth/access_token/get?partner_id=$partner_id&timestamp=$timestamp&sign=$sign_refresh", ["headers"=>[
                            'Content-Type' => 'application/json',
                        ],
                        'json'=> [
                                'shop_id'=>$shop_id,
                                'refresh_token'=>$refresh_token,
                                'partner_id'=>$partner_id
                            ]
                        ])->getBody()->getContents();
                    $response = json_decode($resRefresh);
                    $access_token = $response->access_token; 
                    $refresh_token = $response->refresh_token; 
                    $expired_in = $response->expire_in;
                    DB::table('Shopee_shop')->where('shop_id', $shop_id)->update([
                            'chat_access_token'=>$access_token,
                            'chat_refresh_token'=>$refresh_token,
                            'chat_expired_in_second'=>$expired_in,
                            'chat_access_token_created'=>$currentHour,
                        ]);
                }
                
                $timestamp = time();
                $path = "/api/v2/sellerchat/get_unread_conversation_count";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
                $clientChat = new Client();
                $resChatCount = $clientChat->get("https://partner.shopeemobile.com/api/v2/sellerchat/get_unread_conversation_count?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign")->getBody()->getContents();
                $responseChatCount = json_decode($resChatCount);
                $number = $responseChatCount->response->total_unread_count;
                // dd($number);
                if($number>0){
                    $config['content'] = "Chat Masuk di $shop->name Shopee";
                    $config['to'] = "https://shopee.co.id/";
                    $config['id_cms_users'] = $notifSendTo;
                    \CRUDBooster::sendNotification($config);
                }
                $path = "/api/v2/sellerchat/get_conversation_list";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
            // message list yang unread
                $resChat = $clientChat->get("https://partner.shopeemobile.com/api/v2/sellerchat/get_conversation_list?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign&direction=latest&page_size=60&type=all")->getBody()->getContents();
                $responseChat = json_decode($resChat);
                // dd($responseChat, $responseChatCount);

               
            }catch (\GuzzleHttp\Exception\ClientException $e){
                // dd(json_decode($e->getResponse()->getBody()->getContents()));
                // dd($e);
            }
            return 'success';
        }
        
    }
}
